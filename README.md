# peachShop

Simple react ui which calls peachStore services

## How to start

- inside the peachshop/client run npm start and it will open browser in localhost:3000

## What you can do in this app:

### Create a product by specifying its name, description, type and amount.

- **name, description, amount and type are mandatory.**
- **name should be string and less than 30 characters.**
- **description should be string and less than 100 characters.**
- **amount should be int.**
- **type only accepted from shown list.**

### update a product whit its id.

### delete product with its id.

- You can see services api in peachStore[https://gitlab.com/smoyk/peachstore]
