import {
  GET_PRODUCTS,
  GET_PRODUCT,
  CREATE_PRODUCT,
  DELETE_PRODUCT,
  UPDATE_PRODUCT
} from "../actions/types.js";
const initialState = {
  products: [],
  product: {},
  total: 0
};
export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCTS: {
      return {
        ...state,
        products: action.payload,
        total: action.payload.length
      };
    }
    case CREATE_PRODUCT: {
      return {
        ...state,
        product: action.payload,
        total: state.total + 1
      };
    }
    case GET_PRODUCT: {
      return { ...state, product: action.payload, products: state.products };
    }
    case DELETE_PRODUCT: {
      return {
        ...state,
        total: state.total - 1,
        product: {},
        products: state.products.filter(p => p.id !== action.payload.id)
      };
    }
    case UPDATE_PRODUCT: {
      const new_products = state.products.filter(
        p => p.id !== action.payload.id
      );
      new_products.unshift(action.payload);
      return {
        total: state.total,
        product: action.payload,
        products: new_products
      };
    }
    default:
      return state;
  }
}
