import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getProducts,
  getProduct,
  deleteProduct,
  updateProduct
} from "../../actions/productActions";
import {withAlert} from 'react-alert'

import { Button, Table } from "react-bootstrap";
import plant from "../../assets/plant.png";
import digital from "../../assets/digital.png";
import veg from "../../assets/veg.png";
import cloth from "../../assets/cloth.png";
import toy from "../../assets/toy.png";
import other from "../../assets/product.png";

class Products extends Component {
  constructor(props){
    super(props);
    this.failureCallback= this.failureCallback.bind(this)
    this.successCallback= this.successCallback.bind(this)
  }
  componentWillMount() {
    this.props.getProducts(this.failureCallback);
  }
  
  componentWillReceiveProps(nextProps) {
    const new_id = nextProps.newProduct.id;
    const ids = this.props.products.map(item => item.id);
    if (nextProps.newProduct && !ids.includes(new_id) && nextProps.newProduct.id) {
      this.props.products.unshift(nextProps.newProduct);
    }
  }
  successCallback(id){this.props.alert.success(`Product ${id} Deleted`)}
  failureCallback(msg){this.props.alert.error(msg)}
  
  onDelete(item) {
    this.props.deleteProduct(item, this.successCallback, this.failureCallback);
  }
  onEdit(item) {
    this.props.getProduct(item, this.failureCallback);
  }
  render() {
    const types = { plant, digital, veg, cloth, toy, other };
    return (
      <div className="">
        <Table striped bordered condensed responsive hover>
          <tbody className="striped bordered">
            <tr>
              <th />
              {["name", "type", "amount", "id", "date"].map((item, key) => {
                return <th key={key}>{item}</th>;
              })}
              <th>Edit</th>
              <th>Delete</th>
            </tr>
            {this.props.products.map((item, key) => {
              return (
                <tr key={key}>
                  <td>
                    <img
                      className="product-icon"
                      src={types[item.type]}
                      alt={item.type}
                    />
                  </td>
                  {["name", "type", "amount", "id", "date"].map(key => {
                    return (
                      <td className="col-xs-2" key={key}>
                        {item[key]}
                      </td>
                    );
                  })}
                  <td>
                    <Button
                      value={key}
                      className="btn btn-small btn-primary"
                      onClick={this.onEdit.bind(this, item)}
                    >
                      edit
                    </Button>
                  </td>
                  <td>
                    <Button
                      value={key}
                      className="btn btn-small btn-warning"
                      onClick={this.onDelete.bind(this, item)}
                    >
                      delete
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  products: state.reducer.products,
  newProduct: state.reducer.product
});
export default connect(
  mapStateToProps,
  { getProducts, getProduct, deleteProduct, updateProduct }
)(withAlert(Products));
