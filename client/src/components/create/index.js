import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";
import { withAlert } from "react-alert";
import { createProduct, updateProduct } from "../../actions/productActions";
import moment from "moment";
class CreateProduct extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onReset = this.onReset.bind(this);
    this.failureCallback = this.failureCallback.bind(this);
    this.successCreateCallback = this.successCreateCallback.bind(this);
    this.successUpdateCallback = this.successUpdateCallback.bind(this);

    this.state = {
      name: "",
      description: "",
      type: "",
      amount: 0,
      date: moment().format("YYYY/MM/DD hh:mm:ss")
    };
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.product && nextProps.product.id) {
      this.setState(nextProps.product);
    }
  }
  onReset(e) {
    if (e) e.preventDefault();
    this.setState({
      name: "",
      description: "",
      type: "",
      amount: 0,
      id: "",
      date: moment().format("YYYY/MM/DD hh:mm:ss")
    });
  }

  onSave(e) {
    e.preventDefault();
    const product = {
      name: this.state.name,
      description: this.state.description,
      type: this.state.type ? this.state.type : "veg",
      amount: this.state.amount,
      date: moment().format("YYYY/MM/DD hh:mm:ss")
    };
    if (this.state.id) {
      product["id"] = this.state.id;
      this.props.updateProduct(
        product,
        this.successUpdateCallback,
        this.failureCallback
      );
    } else {
      this.props.createProduct(
        product,
        this.successCreateCallback,
        this.failureCallback
      );
    }
  }
  successCreateCallback() {
    this.props.alert.success(`Product ${this.state.id} Created`);
  }
  successUpdateCallback() {
    this.props.alert.success(`Product ${this.state.id} Updated`);
  }
  failureCallback(msg) {
    this.props.alert.error(msg);
  }
  render() {
    return (
      <div className="panel panel-default">
        <form className="panel-body">
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="name">Product Name</label>
              <input
                type="text"
                className="form-control"
                name="name"
                id="name"
                value={this.state.name}
                placeholder="Product Name"
                onChange={this.onChange}
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="amount">Amount($)</label>
              <input
                type="text"
                className="form-control"
                id="amount"
                value={this.state.amount}
                name="amount"
                placeholder="Amount"
                onChange={this.onChange}
              />
            </div>
          </div>

          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="description">Description</label>
              <input
                type="text"
                className="form-control"
                id="description"
                name="description"
                value={this.state.description}
                placeholder="Describe this product ..."
                onChange={this.onChange}
              />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="type">Type</label>
              <select
                defaultValue={this.state.type}
                onChange={this.onChange}
                id="type"
                name="type"
                className="form-control"
              >
                <option>Choose...</option>
                {["plant", "digital", "veg", "cloth", "toy", "other"].map(
                  type => {
                    return (
                      <option
                        key={type}
                        value={type}
                        selected={this.state.type === type}
                      >
                        {type}
                      </option>
                    );
                  }
                )}
              </select>
            </div>
            <div className="form-group col-md-1">
              <label className="white">Type</label>
              <Button
                onClick={this.onSave}
                className="btn btn-primary col-md-12"
              >
                Save
              </Button>
            </div>
            <div className="form-group col-md-1">
              <label className="white">Type</label>

              <Button
                onClick={this.onReset}
                className="btn btn-warning col-md-12"
              >
                reset
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product: state.reducer.product
});
export default connect(
  mapStateToProps,
  { createProduct, updateProduct }
)(withAlert(CreateProduct));
