import React, {Component} from "react"
import ReactPaginate from 'react-paginate';

class Paginate extends Component {
  constructor(props) {
    super(props)
    this.setPage = this.setPage.bind(this)
  }

  setPage(data) {
    this.props.onPageChange(data.selected)
  }

  render() {
    return (
      <ReactPaginate previousLabel={"«"}
                     nextLabel={"»"}
                     containerClassName={(this.props.item.pageCount === 0 ? "pagination hidden" : "pagination pagination-lg justify-content-center")}
                     breakClassName={"page-item"}
                     pageClassName={"page-item"}
                     previousClassName="page-item"
                     nextClassName="page-item"
                     breakLabel={<a className="page-link">...</a>}
                     pageLinkClassName="page-link"
                     previousLinkClassName="page-link"
                     nextLinkClassName="page-link"
                     activeClassName={"active"}
                     pageCount={this.props.item.pageCount}
                     forcePage={this.props.item.forcePage}
                     marginPagesDisplayed={this.props.item.marginPages}
                     pageRangeDisplayed={this.props.item.pageRange}
                     onPageChange={this.setPage}
      />
    )
  }
}

export default (Paginate)