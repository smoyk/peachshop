import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";
import { getProducts } from "../../actions/productActions";
import moment from "moment";
class SearchProduct extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  render() {
    return (
      <div className="panel panel-default">
        <form className="panel-body">
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="type">Type</label>
              <select
                onChange={this.onChange}
                id="type"
                name="type"
                className="form-control"
              >
                <option defaultValue>Choose...</option>
                {["plant", "digital", "veg", "cloth", "toy", "other"].map(
                  type => {
                    return (
                      <option key={type} value={type}>
                        {type}
                      </option>
                    );
                  }
                )}
              </select>
            </div>
            <div className="form-group col-md-6">
              <label className="white">...</label>
              <Button
                onClick={this.onSubmit}
                className="btn btn-primary col-md-12"
              >
                Search
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.reducer.products
});
export default connect(
  mapStateToProps,
  { getProducts }
)(SearchProduct);
