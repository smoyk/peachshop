import React, { Component } from "react";
import { Navbar, Nav, NavItem } from "react-bootstrap";
import logo from "../../assets/logo.png";
import { connect } from "react-redux";
class NavbarComponent extends Component {
  componentWillMount() {
    console.log("navbar props", this.props.products);
    console.log("navbar state", this.state);
  }
  render() {
    return (
      <Navbar className="navbar navbar-expand-lg navbar-light bg-light  ">
        <Navbar.Header>
          <Navbar.Brand>
            <img src={logo} className="App-logo" alt="logo" />
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem eventKey={1}>
            <h4>Welcome to Peach products store</h4>
          </NavItem>
          <NavItem>
            <button type="button" className="btn btn-info">
              Products{" "}
              <span className="badge badge-light">{this.props.total}</span>
            </button>
          </NavItem>
        </Nav>
      </Navbar>
    );
  }
}

//export default NavbarComponent;
const mapStateToProps = state => ({
  total: state.reducer.total
});
export default connect(
  mapStateToProps,
  {}
)(NavbarComponent);
