import {
  GET_PRODUCTS,
  GET_PRODUCT,
  CREATE_PRODUCT,
  UPDATE_PRODUCT,
  DELETE_PRODUCT
} from "./types.js";
const server_address = "http://localhost:1024";
export const getProducts = failure => dispatch => {
  fetch(`${server_address}/products`)
    .then(resp => resp.json())
    .then(resp => {
      if (resp.error) {
        failure(resp.message);
      } else {
        dispatch({
          type: GET_PRODUCTS,
          payload: resp.products
        });
      }
    })
    .catch(err => {
      console.log(err.message);
      if (failure) failure(err.message);
    });
};
export const getProduct = (product, failure) => dispatch => {
  fetch(`${server_address}/products/${product.id}`)
    .then(resp => resp.json())
    .then(resp => {
      if (resp.error) {
        failure(resp.message);
      } else {
        dispatch({
          type: GET_PRODUCT,
          payload: resp
        });
      }
    })
    .catch(err => {
      console.log(err);
      if (failure) failure(err.message);
    });
};
export const deleteProduct = (product, success, failure) => dispatch => {
  fetch(`${server_address}/products/${product.id}`, {
    method: "DELETE"
  })
    .then(resp => {
      if (resp.error) {
        failure(resp.message);
      } else {
        dispatch({
          type: DELETE_PRODUCT,
          payload: product
        });
        if (success) success(product.id);
      }
    })
    .catch(err => {
      console.log(err);
      if (failure) failure(err.message);
    });
};

export const createProduct = (product, success, failure) => dispatch => {
  console.log("create", product);
  fetch(`${server_address}/products`, {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(product)
  })
    .then(resp => resp.json())
    .then(resp => {
      if (resp.error) {
        failure(resp.message);
      } else {
        dispatch({
          type: CREATE_PRODUCT,
          payload: resp
        });
        if (success) success();
      }
    })
    .catch(err => {
      console.log("Err in create");
      console.log(err.message);
      if (failure) failure(err.message);
    });
};

export const updateProduct = (product, success, failure) => dispatch => {
  console.log("update", product);
  fetch(`${server_address}/products/${product.id}`, {
    method: "PUT",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(product)
  })
    .then(resp => resp.json())
    .then(resp => {
      if (resp.error) {
        failure(resp.message);
      } else {
        dispatch({
          type: UPDATE_PRODUCT,
          payload: resp
        });
        if (success) success();
      }
    })
    .catch(err => {
      console.log(err);
      if (failure) failure(err.message);
    });
};
